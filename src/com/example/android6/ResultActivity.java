package com.example.android6;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class ResultActivity extends Activity implements OnClickListener {
	EditText text1;
	 Button btnOK;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result);
        text1 = (EditText) findViewById(R.id.editText1);
        btnOK = (Button) findViewById(R.id.aaa);
        btnOK.setOnClickListener(this);
        
    }
    
    
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_result, menu);
        return true;
    }

	public void onClick(View v) {
		Intent intent = new Intent();
        intent.putExtra("nick", text1.getText().toString());
        setResult(RESULT_OK, intent);
        finish();
		
	}
}
