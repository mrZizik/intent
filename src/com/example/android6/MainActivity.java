package com.example.android6;

import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.view.Menu;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

public class MainActivity extends Activity {
	public final int CAMERA_RESULT = 0;
	EditText text;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        
    }
    public void browse(View view) {
    	
    	
    	Intent browseIntent = new Intent(Intent.ACTION_VIEW,
    	Uri.parse("http://www.google.com"));
    	startActivity(browseIntent);
    }
    public void result(View view) {
    	Intent intent = new Intent (this, ResultActivity.class);
    	startActivityForResult(intent, 1);
    	
    }
    public void camera(View view) {
    	Intent cameraIntent = new
    	Intent(MediaStore.ACTION_IMAGE_CAPTURE);
    	startActivityForResult(cameraIntent, CAMERA_RESULT);
 			
    }
    
   
    
    public void dial(View view) {
    	Intent dialIntent = new Intent(Intent.ACTION_CALL,
    	Uri.parse("tel:" + "89634101838"));

    	startActivity(dialIntent);
    }
    
    public void sms(View view) {
    	Intent sms = new Intent(Intent.ACTION_VIEW);
    	Uri.parse("tel:" + "89634101838");
    	sms.setType("vnd.android-dir/mms-sms");
    	sms.putExtra("sms_body", "����� �����-��");
    	startActivity(sms);
    }
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
    	if (data==null) return;
    	String nick = data.getStringExtra("nick");
    	text = (EditText) findViewById(R.id.editText1);
    	text.setText(nick);
    	
    	if (requestCode == CAMERA_RESULT) {

    		Bitmap thumbnail = (Bitmap) data.getExtras().get("data");

    		ImageView ivCamera = (ImageView) findViewById(R.id.imageView1);

    		ivCamera.setImageBitmap(thumbnail);

    		}
    	
    }
    
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_main, menu);
        return true;
    }
}
